from os import path
from typing import *

from pydantic import BaseModel, Field, validator

from . import const
from .utils import read


class TrainConfig(BaseModel):
    """Training configuration used for training process"""

    # Scheduling
    total_steps: int  #: Total number of training steps
    validate_every: int  #: Number of interval steps between validations

    # Data
    train_data: Union[str, List[str]]
    """Path to training data, can be a string for one dataset,
    or a list of mutiple strings for single path or multiple datasets"""
    validate_data: Union[str, List[str]]
    """Path to validation data, can be a string for one dataset,
    or a list of mutiple strings for single path or multiple datasets"""

    # Train process
    lr: float  #: Learning rate

    # Optionals
    dataloader: Dict = Field(default_factory=dict)
    "Keyword arguments for :py:class:`torch.utils.data.DataLoader`"
    print_every: Optional[int] = None
    """Interval steps between terminal logging,
    default to :py:attr:`.validate_data` divided by `5`.
    """

    @validator("print_every", always=True)
    def _print_every(cls, print_every, values):
        if print_every is None:
            print_every = max(values["validate_every"] // 5, 1)
        return print_every

    @classmethod
    def from_file(cls, file_path):
        config = read(file_path)
        return cls.parse_obj(config)


class ModelConfig(BaseModel):
    """Model configuration. All the below input arguments are also properties."""

    backbone: str # N
    """
    Name of the huggingface backbone, will also be used to load weights
    if available. Example: ``"naver-clova-ocr/bros-base-uncased"``
    """

    classes: List[str]
    """A list of information classes. Eg. ``["email", "phone", "name"]``."""

    num_special_classes: int
    """Should be removed, just set it two 3 for now"""

    head_dims: int
    """Number of hidden neurons used by the prediction head"""

    word_embeddings: Optional[str] = None
    """
    Which model to steal the word embedding froms.
    Default to :py:obj:`None`.
    Example: ``vinai/phobert-base``.
    """

    pretrained_weights: Optional[str] = None
    """Path to some weights to load when training. Default to :py:obj:`None`"""

    inference_weights: Optional[str] = None
    """Path to some weights to load for inference. Default to :py:obj:`None`"""

    name: Optional[str] = None
    """The name of the model, used to determine :py:attr:`.best_weight_path`,
    :py:attr:`.latest_weight_path` and :py:attr:`.log_path`.
    """

    @property
    def num_classes(self) -> int:
        """Number of classes"""
        return len(self.classes) + self.num_special_classes

    @classmethod
    def from_file(cls, file_path: str):
        """Initialize a config from config file.

        The file name will be used as the ``name`` key in the config.

        :param file_path: Path to file.
        :returns: A :py:class:`.ModelConfig` object.
        """
        config = read(file_path)
        config["name"] = path.splitext(path.basename(file_path))[0]
        return cls.parse_obj(config)

    @property
    def best_weight_path(self):
        "Path to best weights, used for model check-pointing"
        name = f"{self.name}.{const.best_suffix}.pt"
        return path.join(const.weight_directory, name)

    @property
    def latest_weight_path(self):
        "Path to latest weights, used for model check-pointing"
        name = f"{self.name}.{const.latest_suffix}.pt"
        return path.join(const.weight_directory, name)

    @property
    def log_path(self) -> str:
        "Path to logging dir, used for logging during training"
        return path.join(const.log_path, self.name)
