==================================================
Configurations
==================================================

There are two types of configuration:

* Model configuration: is used to initialize model. `[example] <#model-config-file>`_ `[references] <#model-configurations>`_
* Training configuration: is only used during training. `[example] <#training-config-file>`_ `[references] <#training-configurations>`_

The configuration should be written and stored in ``yaml``.

Examples
==============================

Model config file
------------------------------

Here is an example of a model config file:

.. code-block:: yaml

   # Model config file
   backbone: naver-clova-ocr/bros-base-uncased
   word_embeddings: vinai/phobert-base
   head_dims: 256

   # For continue training
   pretrained_weights: "invoice.pt"

   # For inference
   inference_weights: "invoice-best.pt"

   num_special_classes: 3
   classes:
      - barcode
      - date
      - customer
      - agency
      - representative
      - address
      - id
      - booknum
      - declnum
      - unit
      - quantity
      - tweight
      - kweight
      - signname

Training config file
------------------------------

Here's an example for training config file:

.. code-block:: yaml

   total_steps: 20_000
   validate_every: 200
   lr: 1e-4

   train_data: data/train.json
   validate_data: data/val.json

   dataloader:
     batch_size: 4
     num_workers: 0

Model configurations
==================================================

.. autoclass:: kie.configs.ModelConfig
   :members:
   :exclude-members: model_config, model_fields

Training configurations
==============================

.. autoclass:: kie.configs.TrainConfig
   :members:
   :exclude-members: model_config, model_fields
