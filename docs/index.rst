.. kie-model documentation master file, created by
   sphinx-quickstart on Thu Dec 21 10:00:38 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Key information extraction model
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Table of Contents:

   kie-configurations

Quick start
-----------


To install this library, run:

.. code-block::

   pip install git+https://gitlab.com/.../kie.git


Sample usage:

.. code-block:: python

   from kie import KiePredictor

   model = KiePredictor(...)
   results = model.predict(inputs)

Indices and tables
-----------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
